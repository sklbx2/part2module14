// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayEnhancedInputComponent.h"

UGameplayEnhancedInputComponent::UGameplayEnhancedInputComponent()
{
}

void UGameplayEnhancedInputComponent::RemoveBinds(TArray<uint32>& BindHandles)
{
	for (uint32 Handle : BindHandles)
	{
		RemoveBindingByHandle(Handle);
	}
	BindHandles.Reset();
}