// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayInputConfig.h"

const UInputAction* UGameplayInputConfig::FindNativeInputActionForTag(const FGameplayTag& InputTag) const
{
	for (const FGameplayInputAction& Action : NativeInputActions)
	{
		if (Action.InputAction && (Action.InputTag == InputTag))
		{
			return Action.InputAction;
		}
	}
	UE_LOG(LogTemp, Error, TEXT("Can't find NativeInputAction for InputTag [%s] on InputConfig [%s]."), *InputTag.ToString(), *GetNameSafe(this));

	return nullptr;
}

const UInputAction* UGameplayInputConfig::FindAbilityInputActionForTag(const FGameplayTag& InputTag) const
{
	for (const FGameplayInputAction& Action : AbilityInputActions)
	{
		if (Action.InputAction && (Action.InputTag == InputTag))
		{
			return Action.InputAction;
		}
	}
	UE_LOG(LogTemp, Error, TEXT("Can't find NativeInputAction for InputTag [%s] on InputConfig [%s]."), *InputTag.ToString(), *GetNameSafe(this));

	return nullptr;
}