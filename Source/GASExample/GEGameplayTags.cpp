// Fill out your copyright notice in the Description page of Project Settings.


#include "GEGameplayTags.h"
#include "GameplayTagsManager.h"

FGEGameplayTags FGEGameplayTags::GameplayTags;

void FGEGameplayTags::InitializeNativeTags()
{
	UGameplayTagsManager& Manager = UGameplayTagsManager::Get();

	GameplayTags.AddAllTags(Manager);

	Manager.DoneAddingNativeTags();
}

void FGEGameplayTags::AddAllTags(UGameplayTagsManager& Manager)
{
	// Native Input
	AddTag(Input_Move, "Input.Move", "Move input.");
	AddTag(Input_Look, "Input.Look", "Look input.");

	// Ability Input
	AddTag(Ability_Input_Attack, "Ability.Input.Attack", "Test ability input.");
	AddTag(Ability_Input_Sprint, "Ability.Input.Sprint", "Test ability input.");
}

void FGEGameplayTags::AddTag(FGameplayTag& OutTag, const ANSICHAR* TagName, const ANSICHAR* TagComment)
{
	OutTag = UGameplayTagsManager::Get().AddNativeGameplayTag(FName(TagName), FString(TEXT("(Native) ")) + FString(TagComment));
}
