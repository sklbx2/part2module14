// Fill out your copyright notice in the Description page of Project Settings.


#include "GEAttributeSet.h"

#include "Net/UnrealNetwork.h"

UGEAttributeSet::UGEAttributeSet()
{
}

void UGEAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UGEAttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGEAttributeSet, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGEAttributeSet, Stamina, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UGEAttributeSet, Power, COND_None, REPNOTIFY_Always);
}

void UGEAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	// Custom behavior for specific attributes
	if (Attribute == GetMaxHealthAttribute())
	{
		// If MaxHealth changing -> triggering changes in Health
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}
}

void UGEAttributeSet::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
	const FGameplayAttributeData& MaxAttribute, 
	float NewMaxAttributeValue, 
	const FGameplayAttribute& AffectedAttributeProperty)
{
	// Save the percentage of the �current� attribute to the �maximum� when changing the last one

	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();

	const float CurrentMax = MaxAttribute.GetCurrentValue();

	if (!FMath::IsNearlyEqual(CurrentMax, NewMaxAttributeValue) && AbilityComp)
	{
		if (!FMath::IsNearlyEqual(CurrentMax, 0))
		{
			const float CurrentActual = AffectedAttribute.GetCurrentValue();
			float NewActual = CurrentActual * ( NewMaxAttributeValue / CurrentMax );

			// Set new value
			AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Override, NewActual);
		}
	}
}

void UGEAttributeSet::OnRep_Health(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGEAttributeSet, Health, OldValue);
}

void UGEAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGEAttributeSet, MaxHealth, OldValue);
}

void UGEAttributeSet::OnRep_Stamina(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGEAttributeSet, Stamina, OldValue);
}

void UGEAttributeSet::OnRep_Power(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UGEAttributeSet, Power, OldValue);
}