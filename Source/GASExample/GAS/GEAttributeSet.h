// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"

#include "AbilitySystemComponent.h"

#include "GEAttributeSet.generated.h"


/**
 * This macro defines a set of helper functions for accessing and initializing attributes.
 *
 * The following example of the macro:
 *		ATTRIBUTE_ACCESSORS(ULyraHealthSet, Health)
 * will create the following functions:
 *		static FGameplayAttribute GetHealthAttribute();
 *		float GetHealth() const;
 *		void SetHealth(float NewVal);
 *		void InitHealth(float NewVal);
 */
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)



/**
 * 
 */
UCLASS()
class GASEXAMPLE_API UGEAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:

	UGEAttributeSet();

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;

    UPROPERTY(BlueprintReadOnly, Category = "Health", Replicatedusing = OnRep_Health)
    FGameplayAttributeData Health;
    ATTRIBUTE_ACCESSORS(UGEAttributeSet, Health)

    UPROPERTY(BlueprintReadOnly, Category = "MaxHealth", Replicatedusing = OnRep_MaxHealth)
    FGameplayAttributeData MaxHealth;
    ATTRIBUTE_ACCESSORS(UGEAttributeSet, MaxHealth)

    UPROPERTY(BlueprintReadOnly, Category = "Stamina", Replicatedusing = OnRep_Stamina)
    FGameplayAttributeData Stamina;
    ATTRIBUTE_ACCESSORS(UGEAttributeSet, Stamina)

    UPROPERTY(BlueprintReadOnly, Category = "Power", Replicatedusing = OnRep_Power)
    FGameplayAttributeData Power;
    ATTRIBUTE_ACCESSORS(UGEAttributeSet, Power)

protected:

    void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
        const FGameplayAttributeData& MaxAttribute, float NewMaxAttributeValue,
        const FGameplayAttribute& AffectedAttributeProperty);

    UFUNCTION()
    virtual void OnRep_Health(const FGameplayAttributeData& OldValue);

    UFUNCTION()
    virtual void OnRep_MaxHealth(const FGameplayAttributeData& OldValue);

    UFUNCTION()
    virtual void OnRep_Stamina(const FGameplayAttributeData& OldValue);

    UFUNCTION()
    virtual void OnRep_Power(const FGameplayAttributeData& OldValue);
};
