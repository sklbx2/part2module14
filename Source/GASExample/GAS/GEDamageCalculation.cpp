// Fill out your copyright notice in the Description page of Project Settings.


#include "GEDamageCalculation.h"

#include "AbilitySystemComponent.h"
#include "GEAttributeSet.h"


// Helper struct for fetching the stats necessary for the calculation
struct FDamageStatics
{
	// Capturedef declarations for attributes
	DECLARE_ATTRIBUTE_CAPTUREDEF(Power);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Health);

	// Default constructor
	FDamageStatics()
	{
		// Capturedef definitions for attributes
		DEFINE_ATTRIBUTE_CAPTUREDEF(UGEAttributeSet, Power, Source, false);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UGEAttributeSet, Health, Target, false);
	}
};

// Static helper function to quicly fetch the damage capture attributes
static const FDamageStatics& DamageStatics()
{
	static FDamageStatics DmgStatics;
	return DmgStatics;
}


UGEDamageCalculation::UGEDamageCalculation()
{
	// Add all the capturedef definitions to the list of relevant attrubutes to capture
	RelevantAttributesToCapture.Add(DamageStatics().PowerDef);
	RelevantAttributesToCapture.Add(DamageStatics().HealthDef);
}

// Do the damage calculations and modify health accordingly
void UGEDamageCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	// Obtain references to the target and source Ability System Component
	
	UAbilitySystemComponent* TargetASC = ExecutionParams.GetTargetAbilitySystemComponent();
	AActor* TargetActor = TargetASC ? TargetASC->GetAvatarActor() : nullptr;

	UAbilitySystemComponent* SourceASC = ExecutionParams.GetSourceAbilitySystemComponent();
	AActor* SourceActor = SourceASC ? SourceASC->GetAvatarActor() : nullptr;

	// Get the owning GameplayEffect Spec so that you can use its variables and tags
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();

	// Aggregator Evaluate Parameters used during the attribute capture
	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	// Capturing parameter by SetByCallerMagnitude
	// float Damage = FMath::Max<float>(Spec.GetSetByCallerMagnitude(FGameplayTag::RequestGameplayTag(FName("Data.Damage")), false, -1.f), 0.f);

	// Capturing Power
	float Power = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().PowerDef, EvaluationParameters, Power);

	// Getting Level
	float Level = ExecutionParams.GetOwningSpec().GetLevel();

	// Result
	float DamageDone = Power * Level;

	// Final execution output. We can add more than one AddOutputModifier to change multiple parameters at a time
	// based on more calculations.
	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatics().HealthProperty, 
		EGameplayModOp::Additive, 
		/*negate here*/-DamageDone));

}
