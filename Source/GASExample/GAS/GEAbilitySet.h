// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "GameplayTagContainer.h"
#include "GameplayAbilitySpecHandle.h"

#include "AttributeSet.h"
#include "ActiveGameplayEffectHandle.h"

#include "GEAbilitySet.generated.h"

class UAttributeSet;
class UGameplayEffect;
class UGEAbilitySystemComponent;
class UGEGameplayAbility;

/**
 *	Data used by the ability set to grant gameplay abilities.
 */
USTRUCT(BlueprintType)
struct FGEAbilitySet_GameplayAbility
{
	GENERATED_BODY()

public:
	// Gameplay ability to grant.
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UGEGameplayAbility> Ability = nullptr;

	// Level of ability to grant.
	UPROPERTY(EditDefaultsOnly)
	int32 AbilityLevel = 1;

	// Tag used to process input for the ability.
	UPROPERTY(EditDefaultsOnly, Meta = (Categories = "InputTag"))
	FGameplayTag InputTag;
};

/**
 *	Data used by the ability set to grant gameplay effects.
 */
 USTRUCT(BlueprintType)
 struct FGEAbilitySet_GameplayEffect
 {
	 GENERATED_BODY()

 public:

	 // Gameplay effect to grant.
	 UPROPERTY(EditDefaultsOnly)
	 TSubclassOf<UGameplayEffect> GameplayEffect = nullptr;

	 // Level of gameplay effect to grant.
	 UPROPERTY(EditDefaultsOnly)
	 float EffectLevel = 1.0f;
 };

 /**
  *	Data used by the ability set to grant attribute sets.
  */
  USTRUCT(BlueprintType)
  struct FGEAbilitySet_AttributeSet
  {
	  GENERATED_BODY()

  public:
	  // Gameplay attribute to grant.
	  UPROPERTY(EditDefaultsOnly)
	  TSubclassOf<UAttributeSet> AttributeSet;

  };

  /**
   *	Data used to store handles to what has been granted by the ability set.
   */
USTRUCT(BlueprintType)
struct FGEAbilitySet_GrantedHandles
{
	GENERATED_BODY()

public:
	void AddAbilitySpecHandle(const FGameplayAbilitySpecHandle& Handle);
	void AddGameplayEffectHandle(const FActiveGameplayEffectHandle& Handle);
	void AddAttributeSet(UAttributeSet* Set);

protected:

	// Handles to the granted abilities.
	UPROPERTY()
	TArray<FGameplayAbilitySpecHandle> AbilitySpecHandles;

	// Handles to the granted gameplay effects.
	UPROPERTY()
	TArray<FActiveGameplayEffectHandle> GameplayEffectHandles;

	// Pointers to the granted attribute sets
	UPROPERTY()
	TArray<TObjectPtr<UAttributeSet>> GrantedAttributeSets;

};

/**
 * Non-mutable data asset used to grant gameplay abilities and gameplay effects.
 */
UCLASS(BlueprintType, Const)
class GASEXAMPLE_API UGEAbilitySet : public UDataAsset
{
	GENERATED_BODY()
	

public:

	// Grants the ability set to the specified ability system component.
	// The returned handles can be used later to take away anything that was granted.
	void GiveToAbilitySystem(UGEAbilitySystemComponent* InASC, FGEAbilitySet_GrantedHandles* OutGrantedHandles, UObject* SourceObject = nullptr) const;

protected:

	// Gameplay abilities to grant when this ability set is granted.
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay Abilities", meta = (TitleProperty = Ability))
	TArray<FGEAbilitySet_GameplayAbility> GrantedGameplayAbilities;

	// Gameplay effects to grant when this ability set is granted.
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay Effects", meta=(TitleProperty=GameplayEffect))
	TArray<FGEAbilitySet_GameplayEffect> GrantedGameplayEffects;

	// Attribute sets to grant when this ability set is granted.
	UPROPERTY(EditDefaultsOnly, Category = "Attribute Sets", meta=(TitleProperty=AttributeSet))
	TArray<FGEAbilitySet_AttributeSet> GrantedAttributes;
};
