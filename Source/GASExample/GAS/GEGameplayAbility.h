// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"


#include "GEGameplayAbility.generated.h"

/**
 * Defines how ability is meant to activate
 */
UENUM(BlueprintType)
enum class EGEAbilityActivationPolicy : uint8
{
	// Try to activate the ability when the input is triggered.
	OnInputTriggered,

	// Continually try to activate the ability while the input is active.
	WhileInputActive
};

/**
 *	The base gameplay ability class used by this GEect.
 */
UCLASS(Abstract, HideCategories = Input, Meta = (ShortTooltip = "The base gameplay ability class used by this GEect."))
class GASEXAMPLE_API UGEGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:

	EGEAbilityActivationPolicy GetActivationPolicy() const { return ActivationPolicy; }

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ability Activation")
	EGEAbilityActivationPolicy ActivationPolicy;

};
