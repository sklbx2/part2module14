// Fill out your copyright notice in the Description page of Project Settings.


#include "GEAssetManager.h"
#include "AbilitySystemGlobals.h"
#include "GEGameplayTags.h"

UGEAssetManager::UGEAssetManager()
{
}

UGEAssetManager& UGEAssetManager::Get()
{
	check(GEngine)
		if (UGEAssetManager* Singleton = Cast<UGEAssetManager>(GEngine->AssetManager))
		{
			return *Singleton;
		}

	UE_LOG(LogTemp, Fatal, TEXT("Invalid AssetManagerClassName in DefaultEndine.ini. It must be set to ISAssetManager!"))

		return *NewObject<UGEAssetManager>();
}

void UGEAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();

	FGEGameplayTags::InitializeNativeTags(); // Initializing our tags

	UAbilitySystemGlobals::Get().InitGlobalData();
}
