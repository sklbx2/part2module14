// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"


#include "GEAssetManager.generated.h"

/**
 * 
 */
UCLASS()
class GASEXAMPLE_API UGEAssetManager : public UAssetManager
{
	GENERATED_BODY()

public:

	UGEAssetManager();

	static UGEAssetManager& Get();

	// Overrided to init our tags
	virtual void StartInitialLoading() override;
	
};
