// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

class UGameplayTagsManager;

struct GASEXAMPLE_API FGEGameplayTags
{
public:
	static const FGEGameplayTags& Get() { return GameplayTags; }
	static void InitializeNativeTags();

public:
	// Native Input
	FGameplayTag Input_Move;
	FGameplayTag Input_Look;

	// Ability Input
	FGameplayTag Ability_Input_Attack;
	FGameplayTag Ability_Input_Sprint;


protected:
	void AddAllTags(UGameplayTagsManager& Manager);
	void AddTag(FGameplayTag& OutTag, const ANSICHAR* TagName, const ANSICHAR* TagComment);

private:
	static FGEGameplayTags GameplayTags;
};
