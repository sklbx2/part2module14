// Fill out your copyright notice in the Description page of Project Settings.


#include "GEPlayerState.h"
#include "../GAS/GEAbilitySystemComponent.h"
//#include "UGESomeSet.h"

AGEPlayerState::AGEPlayerState()
{
	// Create ability system component, set it to be replicated
	AbilitySystemComponent = CreateDefaultSubobject<UGEAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Mixed mode means we only are replicated the GEs to ourself, not the GEs to simulated proxies.
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	// Set PlayerState's NetUpdateFrequency to the same as the Character,
	// default is very low and will introduce perceived lag in the ability system.
	NetUpdateFrequency = 100.0f;

	// These attribute sets will be detected by AbilitySystemComponent::InitializeComponent. Keeping a reference so that the sets don't get garbage collected before that.
	//SomeSet = CreateDefaultSubobject<UGESomeSet>(TEXT("SomeSet"));
}

UAbilitySystemComponent* AGEPlayerState::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}
