// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"


#include "GEPlayerController.generated.h"

class UGEAbilitySystemComponent;
class AGEPlayerState;

/**
 *	The base player controller class used by this project.
 */
UCLASS(Config = Game, Meta = (ShortTooltip = "The base player controller class used by this project."))
class GASEXAMPLE_API AGEPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	AGEPlayerController();

	UFUNCTION(BlueprintCallable, Category = "PlayerController")
	AGEPlayerState* GetGEPlayerState() const;

	UFUNCTION(BlueprintCallable, Category = "GEAbilitySystemComponent")
	UGEAbilitySystemComponent* GetGEAbilitySystemComponent() const;

protected:

	//~APlayerController interface
	virtual void PreProcessInput(const float DeltaTime, const bool bGamePaused) override;
	virtual void PostProcessInput(const float DeltaTime, const bool bGamePaused) override;
	//~End of APlayerController interface

};
