// Fill out your copyright notice in the Description page of Project Settings.


#include "GEPlayerController.h"
#include "../GAS/GEAbilitySystemComponent.h"
#include "GEPlayerState.h"

AGEPlayerController::AGEPlayerController()
{
}

AGEPlayerState* AGEPlayerController::GetGEPlayerState() const
{
	return CastChecked<AGEPlayerState>(PlayerState, ECastCheckedType::NullAllowed);
}

UGEAbilitySystemComponent* AGEPlayerController::GetGEAbilitySystemComponent() const
{
	const AGEPlayerState* PS = GetGEPlayerState();
	return (PS ? CastChecked<UGEAbilitySystemComponent>(PS->GetAbilitySystemComponent()) : nullptr);
}

void AGEPlayerController::PreProcessInput(const float DeltaTime, const bool bGamePaused)
{
	Super::PreProcessInput(DeltaTime, bGamePaused);
}

void AGEPlayerController::PostProcessInput(const float DeltaTime, const bool bGamePaused)
{
	if (UGEAbilitySystemComponent* ASC = GetGEAbilitySystemComponent())
	{
		ASC->ProcessAbilityInput(DeltaTime, bGamePaused);
	}

	Super::PostProcessInput(DeltaTime, bGamePaused);
}