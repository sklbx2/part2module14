// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AbilitySystemInterface.h"

#include "GEPlayerState.generated.h"

class UGEAbilitySystemComponent;

/**
 *	Base player state class used by this project.
 */
UCLASS(Config = Game)
class GASEXAMPLE_API AGEPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:

	AGEPlayerState();

	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

protected:

	// The ability system component sub-object used by player characters.
	UPROPERTY(VisibleAnywhere, Category = "PlayerState")
	TObjectPtr<UGEAbilitySystemComponent> AbilitySystemComponent;

	// Some attribute set used by this actor.
	/*UPROPERTY()
	TObjectPtr<const class UGESomeSet> SomeSet;*/
};
